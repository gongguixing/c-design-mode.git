#ifndef ITARGET_H
#define ITARGET_H

//目标接口
class ITarget
{
protected:
    virtual ~ITarget() {}

public:
    virtual void request() = 0;
};

#endif // ITARGET_H
