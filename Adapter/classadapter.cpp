#include "classadapter.h"
#include "iostream"

ClassAdapter::ClassAdapter()
{

}

void ClassAdapter::request()
{
    std::cout << "I am the ClassAdapter." << std::endl;
    specificRequest();
}
