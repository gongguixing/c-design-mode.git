#ifndef ADAPTEE_H
#define ADAPTEE_H

//适配者接口
class Adaptee
{
public:
    Adaptee();
    void specificRequest();
};

#endif // ADAPTEE_H
