#include "objectadapter.h"
#include "iostream"

ObjectAdapter::ObjectAdapter()
{
    m_pAdaptee = new Adaptee();
}

ObjectAdapter::~ObjectAdapter()
{
    delete m_pAdaptee;
}

void ObjectAdapter::request()
{
    std::cout << "I am the ObjectAdapter." << std::endl;
    m_pAdaptee->specificRequest();
}
