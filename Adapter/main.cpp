#include <iostream>
#include "classadapter.h"
#include "objectadapter.h"

int main()
{
    // 适配器的作用是将一个类的接口转换成客户希望的另外一个接口

    // 通过类适配器类，访问目标接口
    ClassAdapter *pCA = new ClassAdapter();
    pCA->request();
    delete pCA;

    std::cout << std::endl;

    // 通过对象适配器类，访问目标接口
    ObjectAdapter *pOA = new ObjectAdapter();
    pOA->request();
    delete pOA;

    return 0;
}
