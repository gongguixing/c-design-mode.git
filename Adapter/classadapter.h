#ifndef CLASSADAPTER_H
#define CLASSADAPTER_H

#include "itarget.h"
#include "adaptee.h"

//类适配器类,通过继承方式得到适配者的方法
class ClassAdapter : public ITarget, protected Adaptee
{
public:
    ClassAdapter();
    void request() override;
};

#endif // CLASSADAPTER_H
