#ifndef OBJECTADAPTER_H
#define OBJECTADAPTER_H

#include "itarget.h"
#include "adaptee.h"

//对象适配器类，通过成员变量方式获取对象方法
class ObjectAdapter : public ITarget
{
public:
    ObjectAdapter();
    ~ObjectAdapter();

    void request() override;

private:
    Adaptee *m_pAdaptee;
};

#endif // OBJECTADAPTER_H
