#include "csimplefactory.h"
#include "apple.h"
#include "banana.h"
#include "pear.h"

CSimpleFactory::CSimpleFactory()
{

}

IFruit *CSimpleFactory::makeFruit(const string &name)
{
    // 根据传入参数name生产水果
    if (name.compare("apple") == 0)
    {
        // 生产苹果
        return new Apple();
    }
    else if (name.compare("banana") == 0)
    {
        // 生产香蕉
        return new Banana();
    }
    else if (name.compare("pear") == 0)
    {
        // 生产梨子
        return new Pear();
    }

    return NULL;
}
