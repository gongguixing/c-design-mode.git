#ifndef BANANA_H
#define BANANA_H

#include "ifruit.h"

class Banana : public IFruit
{
public:
    Banana();

    // 重写show方法
    void show() override;

};

#endif // BANANA_H
