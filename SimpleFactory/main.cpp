#include <iostream>
#include "csimplefactory.h"

int main()
{
    // 告诉工厂生产苹果
    IFruit *a = CSimpleFactory::makeFruit("apple");
    a->show();

    // 告诉工厂生产香蕉
    IFruit *b = CSimpleFactory::makeFruit("banana");
    b->show();

    // 告诉工厂生产梨子
    IFruit *c = CSimpleFactory::makeFruit("pear");
    c->show();

    // 创建了对象记得释放，delete
    return 0;
}
