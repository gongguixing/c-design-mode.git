#ifndef PEAR_H
#define PEAR_H

#include "ifruit.h"

class Pear : public IFruit
{
public:
    Pear();

    // 重写show方法
    void show() override;
};

#endif // PEAR_H
