#ifndef CSIMPLEFACTORY_H
#define CSIMPLEFACTORY_H

#include "ifruit.h"
#include "string"

using namespace std;

class CSimpleFactory
{
public:
    CSimpleFactory();
    // 生产水果接口
    static IFruit * makeFruit(const string &name);
};

#endif // CSIMPLEFACTORY_H
