#ifndef IDRINKTEMPLATE_H
#define IDRINKTEMPLATE_H

// 抽象类/抽象模板（Abstract Class）
// 负责给出一个算法的轮廓和骨架。它由一个模板方法和若干个基本方法构成。
// 模板方法：定义了算法的骨架，按某种顺序调用其包含的基本方法。
// 基本方法：是整个算法中的一个步骤，包含以下几种类型。
// 抽象方法：在抽象类中声明，由具体子类实现。
// 具体方法：在抽象类中已经实现，在具体子类中可以继承或重写它。
// 钩子方法：在抽象类中已经实现，包括用于判断的逻辑方法和需要子类重写的空方法两种。

class IDrinkTemplate
{
public:
    IDrinkTemplate();
    virtual ~IDrinkTemplate();

    // 抽象方法，推迟到子类实现
    // 煮开水
    virtual void boilWater()    = 0;
    // 冲泡
    virtual void brew()         = 0;
    // 倒入杯中
    virtual void poulInCup()    = 0;
    // 加料
    virtual void addSomething() = 0;

    // 定义一个操作中的算法骨架,模板方法
    // 制作饮料
    virtual void make();
};

#endif // IDRINKTEMPLATE_H
