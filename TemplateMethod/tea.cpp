#include "tea.h"
#include <iostream>

Tea::Tea()
{

}

void Tea::boilWater()
{
    std::cout << "Boiled mineral water." << std::endl;
}

void Tea::brew()
{
    std::cout << "Brewing tea." << std::endl;
}

void Tea::poulInCup()
{
    std::cout << "Pour into a teacup." << std::endl;
}

void Tea::addSomething()
{
    std::cout << "Add tea to tea." << std::endl;
}

void Tea::make()
{
    // 调用父类方法
    IDrinkTemplate::make();
    // 加自己额外的步骤
    std::cout << "Tea is made." << std::endl;
}
