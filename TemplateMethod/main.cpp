#include <iostream>
#include "coffee.h"
#include "tea.h"

int main()
{
    //模板方法（Template Method）模式的定义如下：
    //定义一个操作中的算法骨架，而将算法的一些步骤延迟到子类中，
    //使得子类可以不改变该算法结构的情况下重定义该算法的某些特定步骤。

    IDrinkTemplate * pCoffee = new Coffee();
    // 模板方法调用
    pCoffee->make();
    delete pCoffee;

    std::cout << std::endl;

    IDrinkTemplate * pTea = new Tea();
    // 模板方法调用
    pTea->make();
    delete pTea;

    return 0;
}
