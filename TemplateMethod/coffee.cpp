#include "coffee.h"
#include <iostream>

Coffee::Coffee()
{

}

void Coffee::boilWater()
{
    std::cout << "Boiled mountain spring water." << std::endl;
}

void Coffee::brew()
{
    std::cout << "Brewing coffee." << std::endl;
}

void Coffee::poulInCup()
{
    std::cout << "Pour into a coffee cup." << std::endl;
}

void Coffee::addSomething()
{
    std::cout << "Add milk to coffee." << std::endl;
}

void Coffee::make()
{
    // 调用父类方法
    IDrinkTemplate::make();
    // 加自己额外的步骤
    std::cout << "Coffee is made." << std::endl;
}
