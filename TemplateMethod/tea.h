#ifndef TEA_H
#define TEA_H

#include "idrinktemplate.h"

// 具体实现类，实现抽象类中所定义的抽象方法和钩子方法，
// 它们是一个顶级逻辑的一个组成步骤。
class Tea : public IDrinkTemplate
{
public:
    Tea();

    // 重写父类抽象方法
    // 煮开水
    void boilWater()    override;
    // 冲泡
    void brew()         override;
    // 倒入杯中
    void poulInCup()    override;
    // 加料
    void addSomething() override;

    // 可以重写父类模板方法，加上自己的一些内容
    void make() override;
};

#endif // TEA_H
