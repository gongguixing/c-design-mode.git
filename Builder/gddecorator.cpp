#include "gddecorator.h"

GDDecorator::GDDecorator()
{

}

void GDDecorator::buildWall()
{
    this->m_pParlour->setWall("GD wall");
}

void GDDecorator::buildTV()
{
    this->m_pParlour->setTV("GD TV");
}

void GDDecorator::buildSofa()
{
    this->m_pParlour->setSofa("GD sofa");
}
