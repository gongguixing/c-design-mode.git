#include "gxdecorator.h"

GXDecorator::GXDecorator()
{

}

void GXDecorator::buildWall()
{
    this->m_pParlour->setWall("GX wall");
}

void GXDecorator::buildTV()
{
    this->m_pParlour->setTV("GX TV");
}

void GXDecorator::buildSofa()
{
    this->m_pParlour->setSofa("GX sofa");
}
