#include "parlour.h"
#include "iostream"

Parlour::Parlour()
{

}

void Parlour::setWall(const string &wall)
{
    m_wall = wall;
}

void Parlour::setTV(const string &TV)
{
    m_TV = TV;
}

void Parlour::setSofa(const string &sofa)
{
    m_sofa = sofa;
}

void Parlour::show()
{
    std::cout << "My Parlour" << std::endl;
    std::cout << m_wall << std::endl;
    std::cout << m_TV << std::endl;
    std::cout << m_TV << std::endl;
}
