#include <iostream>
#include "gddecorator.h"
#include "gxdecorator.h"
#include "projectmanager.h"
#include "parlour.h"

int main()
{
    // 具体建造者：具体装修工人GD ，builder
    AbstractDecorator *pABGD = new GDDecorator();
    // 指挥者：项目经理
    ProjectManager *pPMGD = new ProjectManager(pABGD);
    // 产品构建与组装
    Parlour * pGD = pPMGD->decorate();
    // 展示
    pGD->show();

    std::cout << "-------------------" << std::endl;

    // 具体建造者：具体装修工人GX ，builder
    AbstractDecorator *pABGX = new GXDecorator();
    // 指挥者：项目经理
    ProjectManager *pPMGX = new ProjectManager(pABGX);
    // 产品构建与组装
    Parlour * pGX = pPMGX->decorate();
    // 展示
    pGX->show();

    // 记得释放对象 delete
    return 0;
}
