#ifndef PARLOUR_H
#define PARLOUR_H

#include "string"

using namespace std;

//产品：客厅
class Parlour
{
public:
    Parlour();

    void setWall(const string &wall);
    void setTV(const string &TV);
    void setSofa(const string &sofa);
    void show();
private:
    string m_wall;      //墙
    string m_TV;        //电视
    string m_sofa;      //沙发
};

#endif // PARLOUR_H
