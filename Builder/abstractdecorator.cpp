#include "abstractdecorator.h"

AbstractDecorator::AbstractDecorator()
{
    m_pParlour = new Parlour();
}

Parlour *AbstractDecorator::getResult()
{
    return m_pParlour;
}
