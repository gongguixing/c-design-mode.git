#ifndef GDDECORATOR_H
#define GDDECORATOR_H

#include "abstractdecorator.h"

//具体建造者：具体装修工人GD
class GDDecorator : public AbstractDecorator
{
public:
    GDDecorator();

    void buildWall() override;
    void buildTV()  override;
    void buildSofa()override;
};

#endif // GDDECORATOR_H
