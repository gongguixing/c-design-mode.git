#ifndef ABSTRACTDECORATOR_H
#define ABSTRACTDECORATOR_H

#include "parlour.h"

//抽象建造者：装修工人
class AbstractDecorator
{
public:
    AbstractDecorator();
    virtual ~AbstractDecorator() {}

    virtual void buildWall()    = 0;
    virtual void buildTV()      = 0;
    virtual void buildSofa()    = 0;

    Parlour * getResult();

protected:
    Parlour *m_pParlour;
};

#endif // ABSTRACTDECORATOR_H
