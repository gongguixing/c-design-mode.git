#ifndef GXDECORATOR_H
#define GXDECORATOR_H

#include "abstractdecorator.h"

//具体建造者：具体装修工人GX
class GXDecorator : public AbstractDecorator
{
public:
    GXDecorator();

    void buildWall() override;
    void buildTV()  override;
    void buildSofa()override;
};

#endif // GXDECORATOR_H
