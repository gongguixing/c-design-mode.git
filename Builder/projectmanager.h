#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include "abstractdecorator.h"

//指挥者：项目经理
class ProjectManager
{
public:
    ProjectManager(AbstractDecorator *p);
    //产品构建与组装方法
    Parlour *decorate();

private:
    AbstractDecorator * m_pAD;
};

#endif // PROJECTMANAGER_H
