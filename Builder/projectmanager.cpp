#include "projectmanager.h"

ProjectManager::ProjectManager(AbstractDecorator *p)
{
    m_pAD = p;
}

Parlour * ProjectManager::decorate()
{
    m_pAD->buildWall();
    m_pAD->buildTV();
    m_pAD->buildSofa();
    return m_pAD->getResult();
}
