#include "subsystemb.h"
#include <iostream>

SubSystemB::SubSystemB()
{

}

void SubSystemB::run()
{
    std::cout << "I am the SubSystemB," << "    run" << std::endl;
}

void SubSystemB::stop()
{
    std::cout << "I am the SubSystemB," << "    stop" << std::endl;
}
