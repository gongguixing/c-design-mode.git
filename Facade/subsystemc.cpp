#include "subsystemc.h"
#include <iostream>

SubSystemC::SubSystemC()
{

}

void SubSystemC::run()
{
    std::cout << "I am the SubSystemC," << "    run" << std::endl;
}

void SubSystemC::stop()
{
    std::cout << "I am the SubSystemC," << "    stop" << std::endl;
}
