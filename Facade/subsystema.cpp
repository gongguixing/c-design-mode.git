#include "subsystema.h"
#include <iostream>

SubSystemA::SubSystemA()
{

}

void SubSystemA::run()
{
    std::cout << "I am the SubSystemA," << "    run" << std::endl;
}

void SubSystemA::stop()
{
    std::cout << "I am the SubSystemA," << "    stop" << std::endl;
}
