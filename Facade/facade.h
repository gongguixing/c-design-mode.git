#ifndef FACADE_H
#define FACADE_H

class SubSystemA;
class SubSystemB;
class SubSystemC;

//外观角色
//外观（Facade）角色：为多个子系统对外提供一个共同的接口
class Facade
{
public:
    Facade();
    ~Facade();

    //外观角色方法
    void run();
    void stop();

private:
    SubSystemA *m_A;
    SubSystemB *m_B;
    SubSystemC *m_C;
};

#endif // FACADE_H
