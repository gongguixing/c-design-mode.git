#ifndef SUBSYSTEMC_H
#define SUBSYSTEMC_H

//子系统角色C
//子系统（Sub System）角色：实现系统的部分功能，客户可以通过外观角色访问它
class SubSystemC
{
public:
    SubSystemC();

    //子系统角色C方法
    void run();
    void stop();
};

#endif // SUBSYSTEMC_H
