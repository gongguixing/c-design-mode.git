#include <iostream>
#include "facade.h"

int main()
{
    //客户（Client）角色：通过一个外观角色访问各个子系统的功能
    Facade * pF = new Facade();
    pF->run();

    std::cout << std::endl;

    pF->stop();
    delete pF;

    return 0;
}
