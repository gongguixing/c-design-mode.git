#include "facade.h"
#include "subsystema.h"
#include "subsystemb.h"
#include "subsystemc.h"

Facade::Facade()
{
    m_A = new SubSystemA();
    m_B = new SubSystemB();
    m_C = new SubSystemC();
}

Facade::~Facade()
{
    delete m_A;
    delete m_B;
    delete m_C;
}

void Facade::run()
{
    // 外观角色调用子系统方法
    m_A->run();
    m_B->run();
    m_C->run();
}

void Facade::stop()
{
    // 外观角色调用子系统方法
    m_A->stop();
    m_B->stop();
    m_C->stop();
}
