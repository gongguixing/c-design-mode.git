#ifndef SUBSYSTEMB_H
#define SUBSYSTEMB_H

//子系统角色B
//子系统（Sub System）角色：实现系统的部分功能，客户可以通过外观角色访问它
class SubSystemB
{
public:
    SubSystemB();

    //子系统角色C方法
    void run();
    void stop();
};

#endif // SUBSYSTEMB_H
