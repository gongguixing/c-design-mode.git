#ifndef ICOMPONENT_H
#define ICOMPONENT_H

#include <string>
using namespace std;

// 透明方式
// 抽象构件角色
// 抽象构件（Component）角色：
// 它的主要作用是为树叶构件和树枝构件声明公共接口，并实现它们的默认行为。
class IComponent
{
public:
    virtual ~IComponent() {}

    // 添加子对象
    virtual void add(IComponent *c)     = 0;

    // 删除子对象
    virtual void remove(IComponent *c)  = 0;

    // 获取指定子对象
    virtual IComponent* getChild(uint8_t i) = 0;

    // 方法调用
    virtual void operation()            = 0;

    // 释放所有子对象
    virtual void deleteChild()          = 0;

    // 获取唯一标识属性
    virtual string &getKey()            = 0;

    // 获取节点类型
    virtual string &getType()           = 0;
};

#endif // ICOMPONENT_H
