#include "sleaf.h"
#include <iostream>

SLeaf::SLeaf(const string &key)
    : mKey(key)
{
    mType = "SLeaf";
}

void SLeaf::operation()
{
    std::cout << "SLeaf: " << mKey << " operation." << std::endl;
}

string &SLeaf::getKey()
{
    return mKey;
}

string &SLeaf::getType()
{
    return mType;
}
