#ifndef SLEAF_H
#define SLEAF_H

#include "iscomponent.h"

// 安全方式
// 树叶构件角色
// 树叶构件（Leaf）角色：是组合中的叶节点对象，它没有子节点，用于继承或实现抽象构件。
class SLeaf : public ISComponent
{
public:
    SLeaf(const string &key);

    void operation()            override;

    string &getKey()            override;

    string &getType()           override;

private:
    string mKey;
    string mType;
};

#endif // SLEAF_H
