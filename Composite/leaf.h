#ifndef LEAF_H
#define LEAF_H

#include "icomponent.h"

// 透明方式
// 树叶构件角色
// 树叶构件（Leaf）角色：是组合中的叶节点对象，它没有子节点，用于继承或实现抽象构件。
class Leaf : public IComponent
{
public:
    Leaf(const string &key);

    void add(IComponent *c)     override;

    void remove(IComponent *c)  override;

    IComponent* getChild(uint8_t i)override;

    void deleteChild()          override;

    void operation()            override;

    string &getKey()            override;

    string &getType()           override;

private:
    string mKey;
    string mType;
};

#endif // LEAF_H
