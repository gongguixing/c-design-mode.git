#include "composite.h"
#include <iostream>

Composite::Composite(const string &key)
    : mKey(key)
{
    mType = "Composite";
}

void Composite::add(IComponent *c)
{
    mChildren.push_back(c);
    std::cout << "Composite: " << mKey << " add ";
    std::cout << c->getType() << ": " << c->getKey() << std::endl;
}

void Composite::remove(IComponent *c)
{
    uint8_t i = 0;
    for (IComponent *p : mChildren)
    {
        // 地址一致则删除
        if (p == c)
        {
            mChildren.erase(mChildren.begin() + i);
            std::cout << "Composite: " << mKey << " remove ";
            std::cout << c->getType() << ": " << c->getKey() << std::endl;
            break;
        }
        i++;
    }
}

IComponent *Composite::getChild(uint8_t i)
{
    if (i <= mChildren.size())
    {
        return mChildren.at(i);
    }

    return NULL;
}

void Composite::deleteChild()
{
    for (IComponent *p : mChildren)
    {
        std::cout << "Composite: " << mKey << " delete ";
        std::cout << p->getType() << ": " << p->getKey() << std::endl;

        // 树枝构件则释放子构件
        if (p->getType().compare("Composite") == 0)
        {
            p->deleteChild();
            delete p;
        }
        // 叶子构件直接释放
        else if (p->getType().compare("Leef") == 0)
        {
            delete p;
        }
    }

    mChildren.clear();
}

void Composite::operation()
{
    for (IComponent *p : mChildren)
    {
        if (p)
        {
            p->operation();
        }
    }
}

string &Composite::getKey()
{
    return mKey;
}

string &Composite::getType()
{
    return mType;
}
