#include "proxy.h"
#include "iostream"
#include "realsubject.h"

Proxy::Proxy()
{
    m_user = "admin";
    m_passwd =  "123456";
    m_pSubJect = new RealSubject();
}

void Proxy::Request()
{
    Request(user, passwd);
}

void Proxy::setUser(const string &user, const string &passwd)
{
    this->user = user;
    this->passwd = passwd;
}

void Proxy::Request(const string &user, const string &passwd)
{
    preRequest();
    if (!check(user, passwd))
    {
        return ;
    }
    m_pSubJect->Request();
    postRequest();
}

void Proxy::preRequest()
{
    std::cout << "Preprocessing before accessing real topics,"
              << "such as verifying user passwords." << std::endl;
}

void Proxy::postRequest()
{
    std::cout << "Subsequent processing after accessing the real topic." << std::endl;
}

bool Proxy::check(const string &user, const string &passwd)
{
    if (user.compare(m_user))
    {
        std::cout << "user does not exist." << std::endl;
        return false;
    }
    else if (passwd.compare(m_passwd))
    {
        std::cout << "Wrong password." << std::endl;
        return false;
    }

    std::cout << "User authentication succeeded." << std::endl;
    return true;
}
