#ifndef ISUBJECT_H
#define ISUBJECT_H

// 抽象主题
class ISubject
{
protected:
    virtual ~ISubject() {}

public:
    virtual void Request() = 0;
};

#endif // ISUBJECT_H
