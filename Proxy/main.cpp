#include <iostream>
#include "proxy.h"

int main()
{
    Proxy * p = new Proxy;
    // 通过代理Proxy访问真实主题RealSubject，但是未设置用户和密码，访问失败
    p->Request();//方法继承于抽象主题
    std::cout << std::endl;

    //扩展目标对象的功能,设置用户和密码,但是密码错误
    p->setUser("admin", "123");
    p->Request();
    std::cout << std::endl;

    //扩展目标对象的功能,设置用户和密码
    p->setUser("admin", "123456");
    //访问目标对象成功
    p->Request();
    std::cout << std::endl;

    //通过代理Proxy拓展的方法访问真实主题RealSubject
    p->Request("admin", "123456");

    return 0;
}
