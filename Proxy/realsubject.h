#ifndef REALSUBJECT_H
#define REALSUBJECT_H

#include "isubject.h"

// 真实主题
class RealSubject : public ISubject
{
public:
    RealSubject();

    void Request() override;
};

#endif // REALSUBJECT_H
