#ifndef PROXY_H
#define PROXY_H

#include "string"

#include "isubject.h"

using namespace std;

class Proxy : public ISubject
{
public:
    Proxy();

    // 继承抽象主题的方法
    void Request() override;
    // 设置请求用户和密码
    void setUser(const string &user, const string &passwd);

    // 另外实现的方法
    void Request(const string &user, const string &passwd);

protected:
    void preRequest();
    void postRequest();
    bool check(const string &user, const string &passwd);

private:
    ISubject *m_pSubJect;
    string m_user;
    string m_passwd;
    string user;
    string passwd;
};

#endif // PROXY_H
