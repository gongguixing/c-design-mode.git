#ifndef CONCRETEDECORATOR_H
#define CONCRETEDECORATOR_H

#include "idecorator.h"
// 具体装饰角色
// 实现抽象装饰的相关方法，并给具体构件对象添加附加的责任
class ConcreteDecorator : public IDecorator
{
public:
    ConcreteDecorator(IComponent *p);

    void operation() override;

    void addedFunction();
};

#endif // CONCRETEDECORATOR_H
