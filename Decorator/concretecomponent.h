#ifndef CONCRETECOMPONENT_H
#define CONCRETECOMPONENT_H

#include "icomponent.h"

//具体构件
//实现抽象构件，通过装饰角色为其添加一些职责。
class ConcreteComponent : public IComponent
{
public:
    ConcreteComponent();
    void operation() override;
};

#endif // CONCRETECOMPONENT_H
