#include "concretecomponent.h"
#include <iostream>

ConcreteComponent::ConcreteComponent()
{
    mAttribute = 10;
}

void ConcreteComponent::operation()
{
    std::cout << "I am the ConcreteComponent." << std::endl;
    std::cout << "Attribute:    " << mAttribute << std::endl;
}
