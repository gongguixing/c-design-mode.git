#include <iostream>
#include "concretecomponent.h"
#include "concretedecorator.h"

int main()
{
    //装饰器（Decorator）模式的定义：指在不改变现有对象结构的情况下，
    //动态地给该对象增加一些职责（即增加其额外功能）的模式，它属于对象结构型模式。

    // 具体构件
    IComponent *pCC = new ConcreteComponent();
    // 原有职责
    pCC->operation();

    std::cout << std::endl;

    // 如果此处使用原有构件指针，则应在释放原有构件对象
    // 具体装饰
    IComponent *pCD = new ConcreteDecorator(pCC);
    // 为原有构件，添加新属性，新职责
    pCD->operation();

    return 0;
}
