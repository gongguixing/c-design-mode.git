#ifndef ICOMPONENT_H
#define ICOMPONENT_H

//抽象构件角色
//定义一个抽象接口以规范准备接收附加责任的对象。
//定义一个属性，以方便观察动态添加属性
class IComponent
{
public:
    virtual ~IComponent() {}

    virtual void operation() = 0;

    int mAttribute;
};

#endif // ICOMPONENT_H
