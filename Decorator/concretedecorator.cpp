#include "concretedecorator.h"
#include <iostream>

ConcreteDecorator::ConcreteDecorator(IComponent *p)
    : IDecorator(p)
{

}

void ConcreteDecorator::operation()
{
    mAttribute += 100;
    std::cout << "I am the ConcreteDecorator." << std::endl;
    std::cout << "Attribute:    " << mAttribute << std::endl;
    // 添加新职责
    addedFunction();
}

void ConcreteDecorator::addedFunction()
{
    std::cout << "Additional functions." << std::endl;
}
