#ifndef IDECORATOR_H
#define IDECORATOR_H

#include "icomponent.h"

//抽象装饰角色
//继承抽象构件，并包含具体构件的实例，可以通过其子类扩展具体构件的功能
class IDecorator : public IComponent
{
public:
    IDecorator(IComponent *p);

    void operation() override;
};

#endif // IDECORATOR_H
