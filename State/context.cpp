#include "context.h"
#include "concretestatea.h"

Context::Context()
{
    //定义环境类的初始状态
    mState = new ConcreteStateA();
}

Context::~Context()
{
    // 释放最后的状态
    if (mState)
    {
        delete mState;
    }
}

void Context::setState(IState *state)
{
    // 设置新的状态前先示范已有状态
    if (mState)
    {
        delete mState;
        mState = nullptr;
    }

    mState = state;
}

IState *Context::getState()
{
    return mState;
}

void Context::Handle()
{
    if (mState)
    {
        mState->Handle(this);
    }
}
