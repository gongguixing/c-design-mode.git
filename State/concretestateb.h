#ifndef CONCRETESTATEB_H
#define CONCRETESTATEB_H

#include "istate.h"
#include "context.h"

// 具体状态（Concrete State）角色
// 实现抽象状态所对应的行为，并且在需要的情况下进行状态切换。
class ConcreteStateB : public IState
{
public:
    ConcreteStateB();

    void Handle(Context *p) override;

};

#endif // CONCRETESTATEB_H
