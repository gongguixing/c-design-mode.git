#ifndef CONCRETESTATEC_H
#define CONCRETESTATEC_H

#include "istate.h"
#include "context.h"

// 具体状态（Concrete State）角色
// 实现抽象状态所对应的行为，并且在需要的情况下进行状态切换。
class ConcreteStateC : public IState
{
public:
    ConcreteStateC();

    void Handle(Context *p) override;
};

#endif // CONCRETESTATEC_H
