#include "concretestatec.h"
#include "concretestatea.h"
#include <iostream>

ConcreteStateC::ConcreteStateC()
{

}

void ConcreteStateC::Handle(Context *p)
{
    std::cout << "I am the ConcreteStateC." << std::endl;
    // 将状态切换至A
    p->setState(new ConcreteStateA());
}
