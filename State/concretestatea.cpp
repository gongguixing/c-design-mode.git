#include "concretestatea.h"
#include "concretestateb.h"
#include <iostream>

ConcreteStateA::ConcreteStateA()
{

}

void ConcreteStateA::Handle(Context *p)
{
    std::cout << "I am the ConcreteStateA." << std::endl;
    // 将状态切换至B
    p->setState(new ConcreteStateB());
}
