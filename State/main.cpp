#include <iostream>
#include "context.h"

int main()
{
    // 状态（State）模式的定义：对有状态的对象，
    // 把复杂的“判断逻辑”提取到不同的状态对象中，
    // 允许状态对象在其内部状态发生改变时改变其行为。

    // 实例化环境
    Context * p = new Context();
    //对请求做处理
    p->Handle();
    p->Handle();
    p->Handle();
    p->Handle();
    p->Handle();
    p->Handle();
    delete p;

    return 0;
}
