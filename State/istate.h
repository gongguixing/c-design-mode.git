#ifndef ISTATE_H
#define ISTATE_H

class Context;

// 抽象状态（State）角色
// 定义一个接口，用以封装环境对象中的特定状态所对应的行为，可以有一个或多个行为。
class IState
{
public:
    virtual ~IState() {}

    virtual void Handle(Context *p) = 0;
};

#endif // ISTATE_H
