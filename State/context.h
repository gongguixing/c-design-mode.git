#ifndef CONTEXT_H
#define CONTEXT_H

#include "istate.h"

// 环境类（Context）角色
// 也称为上下文，它定义了客户端需要的接口，
// 内部维护一个当前状态，并负责具体状态的切换。
class Context
{
public:
    Context();
    ~Context();

    //设置新状态
    void setState(IState *state);

    //读取状态
    IState * getState();

    //对请求做处理
    void Handle();

private:
    IState * mState;
};

#endif // CONTEXT_H
