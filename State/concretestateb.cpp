#include "concretestateb.h"
#include "concretestatec.h"
#include <iostream>

ConcreteStateB::ConcreteStateB()
{

}

void ConcreteStateB::Handle(Context *p)
{
    std::cout << "I am the ConcreteStateB." << std::endl;
    // 将状态切换至C
    p->setState(new ConcreteStateC());
}
