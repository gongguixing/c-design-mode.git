#ifndef CARETAKER_H
#define CARETAKER_H

class Memento;

// 管理者（Caretaker）角色
// 对备忘录进行管理，提供保存与获取备忘录的功能，
// 但其不能对备忘录的内容进行访问与修改。

class Caretaker
{
public:
    Caretaker();

    // 设置备忘录
    void setMemento(Memento * pM);

    // 获取备忘录
    Memento *getMemento();

private:
    Memento * mPMemento;
};

#endif // CARETAKER_H
