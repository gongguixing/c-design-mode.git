#include "caretaker.h"

Caretaker::Caretaker()
{
    mPMemento = nullptr;
}

void Caretaker::setMemento(Memento *pM)
{
    mPMemento = pM;
}

Memento *Caretaker::getMemento()
{
    return mPMemento;
}
