#ifndef MEMENTO_H
#define MEMENTO_H

#include <string>
using namespace std;

// 备忘录（Memento）角色
// 负责存储发起人的内部状态，在需要的时候提供这些内部状态给发起人。

class Memento
{
public:
    Memento(const string &state);

    // 设置状态
    void setState(const string &state);

    // 获取状态
    string getState() const;

private:
    string mState;
};

#endif // MEMENTO_H
