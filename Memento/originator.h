#ifndef ORIGINATOR_H
#define ORIGINATOR_H

#include <memento.h>

// 发起人（Originator）角色
// 记录当前时刻的内部状态信息，提供创建备忘录和恢复备忘录数据的功能，
// 实现其他业务功能，它可以访问备忘录里的所有信息。

class Originator
{
public:
    Originator();

    // 设置状态
    void setState(const string &state);

    // 获取状态
    string getState() const;

    // 创建备忘录
    Memento  * createMemento();

    // 使用备忘录重置状态
    void restoreMemento(Memento * pM);

private:
    string mState;
};

#endif // ORIGINATOR_H
