#include "originator.h"

Originator::Originator()
{
    mState = "";
}

void Originator::setState(const string &state)
{
    mState = state;
}

string Originator::getState() const
{
    return mState;
}

Memento *Originator::createMemento()
{
    return new Memento(mState);
}

void Originator::restoreMemento(Memento *pM)
{
    mState = pM->getState();
}

