#include <iostream>

#include "originator.h"
#include "caretaker.h"

int main()
{
    // 备忘录（Memento）模式的定义：在不破坏封装性的前提下，
    // 捕获一个对象的内部状态，并在该对象之外保存这个状态，
    // 以便以后当需要时能将该对象恢复到原先保存的状态。该模式又叫快照模式。

    Originator *pOri = new Originator();
    Caretaker *pCar = new Caretaker();

    // 初始状态
    pOri->setState("S0");
    std::cout << "Initial state:    " << pOri->getState() << std::endl;

    // //保存状态
    pCar->setMemento(pOri->createMemento());

    // 新状态
    pOri->setState("S1");
    std::cout << "New status:   " << pOri->getState() << std::endl;

    // //恢复状态
    pOri->restoreMemento(pCar->getMemento());
    std::cout << "Restore state:    " << pOri->getState() << std::endl;

    delete pOri;
    delete pCar;

    return 0;
}
