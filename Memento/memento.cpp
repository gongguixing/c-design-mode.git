#include "memento.h"

Memento::Memento(const string &state)
    :mState(state)
{

}

void Memento::setState(const string &state)
{
    mState = state;
}

string Memento::getState() const
{
    return mState;
}
