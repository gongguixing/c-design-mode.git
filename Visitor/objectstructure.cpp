#include "objectstructure.h"
#include <algorithm>

ObjectStructure::ObjectStructure()
{
    mElement.clear();
}

void ObjectStructure::accept(IVisitor *pV)
{
    for (IElement * pE : mElement)
    {
        if (pE)
        {
            pE->accept(pV);
        }
    }
}

void ObjectStructure::add(IElement *pE)
{
    mElement.push_back(pE);
}

void ObjectStructure::del(IElement *pE)
{
    auto fun = [pE](IElement * p)->bool{return pE == p;};
    mElement.erase(std::remove_if(mElement.begin(), mElement.end(), fun), mElement.end());
}

void ObjectStructure::deleteAll()
{
    for (IElement * pE : mElement)
    {
        if (pE)
        {
            delete pE;
        }
    }
    mElement.clear();
}
