#ifndef OBJECTSTRUCTURE_H
#define OBJECTSTRUCTURE_H

#include "ielement.h"
#include <vector>

// 对象结构（Object Structure）角色
// 是一个包含元素角色的容器，提供让访问者对象遍历容器中的所有元素的方法，
// 通常由 List、Set、Map 等聚合类实现。

class ObjectStructure
{
public:
    ObjectStructure();

    void accept(IVisitor *pV);

    void add(IElement *pE);

    void del(IElement *pE);

    // 释放所有对象
    void deleteAll();

private:
    vector<IElement *> mElement;
};

#endif // OBJECTSTRUCTURE_H
