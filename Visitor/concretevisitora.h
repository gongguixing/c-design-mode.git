#ifndef CONCRETEVISITORA_H
#define CONCRETEVISITORA_H

#include "ivisitor.h"

// 具体访问者（ConcreteVisitor）角色
// 实现抽象访问者角色中声明的各个访问操作，
// 确定访问者访问一个元素时该做什么。

class ConcreteVisitorA : public IVisitor
{
public:
    ConcreteVisitorA();

    void visit(ConcreteElementA *PCA) override;

    void visit(ConcreteElementB *PCB) override;
};

#endif // CONCRETEVISITORA_H
