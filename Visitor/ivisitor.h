#ifndef IVISITOR_H
#define IVISITOR_H

#include <string>
using namespace std;

// 抽象访问者（Visitor）角色
// 定义一个访问具体元素的接口，为每个具体元素类对应一个访问操作 visit() ，
// 该操作中的参数类型标识了被访问的具体元素。

class ConcreteElementA;
class ConcreteElementB;

class IVisitor
{
public:
    virtual ~IVisitor() {}

    virtual void visit(ConcreteElementA *PCA) = 0;

    virtual void visit(ConcreteElementB *PCB) = 0;
};

#endif // IVISITOR_H
