#include "concretevisitorb.h"
#include "concreteelementa.h"
#include "concreteelementb.h"
#include <iostream>

ConcreteVisitorB::ConcreteVisitorB()
{

}

void ConcreteVisitorB::visit(ConcreteElementA *PCA)
{
    std::cout << "具体访问者B访问-->" << PCA->operation() << std::endl;
}

void ConcreteVisitorB::visit(ConcreteElementB *PCB)
{
    std::cout << "具体访问者B访问-->" << PCB->operation() << std::endl;
}
