#ifndef CONCRETEELEMENTB_H
#define CONCRETEELEMENTB_H

#include "ielement.h"

// 具体元素（ConcreteElement）角色
// 实现抽象元素角色提供的 accept() 操作，
// 其方法体通常都是 visitor.visit(this) ，
// 另外具体元素中可能还包含本身业务逻辑的相关操作。

class ConcreteElementB : public IElement
{
public:
    ConcreteElementB();

    void accept(IVisitor *pV) override;

    string operation();
};

#endif // CONCRETEELEMENTB_H
