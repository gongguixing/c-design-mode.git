#include <iostream>
#include "objectstructure.h"
#include "concretevisitora.h"
#include "concretevisitorb.h"
#include "concreteelementa.h"
#include "concreteelementb.h"

int main()
{
    //设置字符集，控制台显示中文
    system("chcp 65001");

    ObjectStructure *pOS = new ObjectStructure();
    pOS->add(new ConcreteElementA());
    pOS->add(new ConcreteElementB());

    IVisitor *pVA = new ConcreteVisitorA();
    pOS->accept(pVA);
    std::cout << "------------------------" << std::endl;
    IVisitor *pVB = new ConcreteVisitorB();
    pOS->accept(pVB);
    return 0;
}
