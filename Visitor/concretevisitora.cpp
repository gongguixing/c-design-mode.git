#include "concretevisitora.h"
#include "concreteelementa.h"
#include "concreteelementb.h"
#include <iostream>

ConcreteVisitorA::ConcreteVisitorA()
{

}

void ConcreteVisitorA::visit(ConcreteElementA *PCA)
{
    std::cout << "具体访问者A访问-->" << PCA->operation() << std::endl;
}

void ConcreteVisitorA::visit(ConcreteElementB *PCB)
{
    std::cout << "具体访问者A访问-->" << PCB->operation() << std::endl;
}
