#ifndef IMEDIATOR_H
#define IMEDIATOR_H

// 抽象中介者（Mediator）角色
// 它是中介者的接口，提供了同事对象注册与转发同事对象信息的抽象方法。

class IColleague;

class IMediator
{
public:
    virtual ~IMediator() {}

    // 对象注册
    virtual void registryObject(IColleague *p)   = 0;

    // 转发
    virtual void relay(IColleague *p)   = 0;
};

#endif // IMEDIATOR_H
