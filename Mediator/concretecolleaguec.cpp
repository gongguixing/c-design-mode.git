#include "concretecolleaguec.h"
#include <iostream>

ConcreteColleagueC::ConcreteColleagueC()
{
    mPMediator = nullptr;
}

void ConcreteColleagueC::setMedium(IMediator *p)
{
    mPMediator = p;
}

void ConcreteColleagueC::receive()
{
    std::cout << "ConcreteColleagueC receive request." << std::endl;
}

void ConcreteColleagueC::send()
{
    std::cout << "ConcreteColleagueC send request." << std::endl;
    // 中介转发请求
    if (mPMediator)
    {
        mPMediator->relay(this);
    }
}
