#ifndef ICOLLEAGUE_H
#define ICOLLEAGUE_H

// 抽象同事类（Colleague）角色
// 定义同事类的接口，保存中介者对象，提供同事对象交互的抽象方法，
// 实现所有相互影响的同事类的公共功能。

class IMediator;

class IColleague
{
public:
    virtual ~IColleague() {}

    // 设置中介对象
    virtual void setMedium(IMediator *p) = 0;

    virtual void receive() = 0;

    virtual void send() = 0;
};

#endif // ICOLLEAGUE_H
