#include <iostream>
#include "concretemediator.h"
#include "concretecolleaguea.h"
#include "concretecolleagueb.h"
#include "concretecolleaguec.h"

int main()
{

    // 中介者（Mediator）模式的定义：
    // 定义一个中介对象来封装一系列对象之间的交互，
    // 使原有对象之间的耦合松散，且可以独立地改变它们之间的交互。
    // 中介者模式又叫调停模式，它是迪米特法则的典型应用。

    // 实例化中介
    IMediator * pMed = new ConcreteMediator();
    // 实例化同事
    IColleague * pCOlA = new ConcreteColleagueA();
    IColleague * pCOlB = new ConcreteColleagueB();
    IColleague * pCOlC = new ConcreteColleagueC();

    // 注册通行对象
    pMed->registryObject(pCOlA);
    pMed->registryObject(pCOlB);
    pMed->registryObject(pCOlC);

    // A发送消息
    pCOlA->send();

    std::cout << "-------------------------------------" << std::endl;

    // B发送消息
    pCOlB->send();

    std::cout << "-------------------------------------" << std::endl;

    // C发送消息
    pCOlC->send();

    return 0;
}
