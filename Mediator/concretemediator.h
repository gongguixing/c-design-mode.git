#ifndef CONCRETEMEDIATOR_H
#define CONCRETEMEDIATOR_H

#include "imediator.h"
#include <vector>
using namespace std;
// 具体中介者（Concrete Mediator）角色
// 实现中介者接口，定义一个 List 来管理同事对象，
// 协调各个同事角色之间的交互关系，因此它依赖于同事角色。

class ConcreteMediator : public IMediator
{
public:
    ConcreteMediator();

    // 对象注册
    void registryObject(IColleague *p)  override;

    // 转发
    void relay(IColleague *p) override;

private:
    vector<IColleague*> mPColleagues;
};

#endif // CONCRETEMEDIATOR_H
