#ifndef CONCRETECOLLEAGUEA_H
#define CONCRETECOLLEAGUEA_H

#include "icolleague.h"
#include "imediator.h"

// 具体同事类（Concrete Colleague）角色
// 是抽象同事类的实现者，当需要与其他同事对象交互时，由中介者对象负责后续的交互。

class ConcreteColleagueA : public IColleague
{
public:
    ConcreteColleagueA();

    void setMedium(IMediator *p) override;

    void receive() override;

    void send() override;

private:
    IMediator *mPMediator;
};

#endif // CONCRETECOLLEAGUEA_H
