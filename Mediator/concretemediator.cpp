#include "concretemediator.h"
#include "icolleague.h"

ConcreteMediator::ConcreteMediator()
{

}

void ConcreteMediator::registryObject(IColleague *p)
{
    if (!p)
    {
        return;
    }

    vector<IColleague*>::iterator it = mPColleagues.begin();
    for (; it != mPColleagues.end(); it++)
    {
        // 对象已经存在不需要重复放入
        if (*it == p)
        {
            return;
        }
    }

    // 同事类将中介设置为当前
    p->setMedium(this);
    // 将同事类存入容器
    mPColleagues.push_back(p);
}

void ConcreteMediator::relay(IColleague *p)
{
    vector<IColleague*>::iterator it = mPColleagues.begin();
    for (; it != mPColleagues.end(); it++)
    {
        // 自己不用处理自己的消息
        if (*it == p)
        {
            continue;
        }

        (*it)->receive();
    }
}
