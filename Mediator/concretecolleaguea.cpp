#include "concretecolleaguea.h"
#include <iostream>

ConcreteColleagueA::ConcreteColleagueA()
{
    mPMediator = nullptr;
}

void ConcreteColleagueA::setMedium(IMediator *p)
{
    mPMediator = p;
}

void ConcreteColleagueA::receive()
{
    std::cout << "ConcreteColleagueA receive request." << std::endl;
}

void ConcreteColleagueA::send()
{
    std::cout << "ConcreteColleagueA send request." << std::endl;
    // 中介转发请求
    if (mPMediator)
    {
        mPMediator->relay(this);
    }
}
