#include "concretecolleagueb.h"
#include <iostream>

ConcreteColleagueB::ConcreteColleagueB()
{
    mPMediator = nullptr;
}

void ConcreteColleagueB::setMedium(IMediator *p)
{
    mPMediator = p;
}

void ConcreteColleagueB::receive()
{
    std::cout << "ConcreteColleagueB receive request." << std::endl;
}

void ConcreteColleagueB::send()
{
    std::cout << "ConcreteColleagueB send request." << std::endl;
    // 中介转发请求
    if (mPMediator)
    {
        mPMediator->relay(this);
    }
}
