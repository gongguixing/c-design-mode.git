#include <iostream>
#include "concretehandler1.h"
#include "concretehandler2.h"
#include "concretehandler3.h"

int main()
{
    IHandler *pH1 = new ConcreteHandler1();
    IHandler *pH2 = new ConcreteHandler2();
    IHandler *pH3 = new ConcreteHandler3();
    pH1->setNext(pH2);
    pH2->setNext(pH3);

    pH1->handleRequest("one");
    pH1->handleRequest("two");
    pH1->handleRequest("three");
    pH1->handleRequest("six");

    return 0;
}
