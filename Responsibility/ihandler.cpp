#include "ihandler.h"

IHandler::IHandler()
{
    mPNext = nullptr;
}

void IHandler::setNext(IHandler *next)
{
    mPNext = next;
}

IHandler *IHandler::getNext()
{
    return mPNext;
}

void IHandler::nextHandle(const string &request)
{
    // 若指针不为空，则说明有下一个处理者
    if (mPNext)
    {
        mPNext->handleRequest(request);
        return;
    }

    std::cout << "No one is handling the request, key: " << request << std::endl;
}
