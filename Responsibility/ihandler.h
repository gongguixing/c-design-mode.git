#ifndef IHANDLER_H
#define IHANDLER_H

#include <string>
#include <iostream>
using namespace std;

// 抽象处理者（Handler）角色
// 定义一个处理请求的接口，包含抽象处理方法和一个后继连接。
class IHandler
{
public:
    IHandler();

    // 设置下一个处理者
    void setNext(IHandler *next);

    // 获取下一个处理者指针
    IHandler * getNext();

    // 将任务派发给下一个处理者
    void nextHandle(const string &request);

    // 抽象接口，处理请求的方法
    virtual void handleRequest(const string &request) = 0;

private:
    IHandler *mPNext;
};

#endif // IHANDLER_H
