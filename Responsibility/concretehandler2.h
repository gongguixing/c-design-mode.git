#ifndef CONCRETEHANDLER2_H
#define CONCRETEHANDLER2_H

#include "ihandler.h"

// 具体处理者（Concrete Handler）角色
// 实现抽象处理者的处理方法，判断能否处理本次请求，
// 如果可以处理请求则处理，否则将该请求转给它的后继者。
class ConcreteHandler2 : public IHandler
{
public:
    ConcreteHandler2();

    //处理请求的方法
    void handleRequest(const string &request) override;
};

#endif // CONCRETEHANDLER2_H
