#include "concretehandler3.h"

ConcreteHandler3::ConcreteHandler3()
{

}

void ConcreteHandler3::handleRequest(const string &request)
{
    // 判断是否是自己处理的请求
    if (request.compare("three") == 0)
    {
        std::cout << "ConcreteHandler3 handle aging requests, key: " << request << std::endl;
        return;
    }

    // 将不是自己的请求通过父类方法传递给下一个
    IHandler::nextHandle(request);
}
