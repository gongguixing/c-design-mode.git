#include "concretehandler1.h"

ConcreteHandler1::ConcreteHandler1()
{

}

void ConcreteHandler1::handleRequest(const string &request)
{
    // 判断是否是自己处理的请求
    if (request.compare("one") == 0)
    {
        std::cout << "ConcreteHandler1 handle aging requests, key: " << request << std::endl;
        return;
    }

    // 将不是自己的请求通过父类方法传递给下一个
    IHandler::nextHandle(request);
}
