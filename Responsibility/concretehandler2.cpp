#include "concretehandler2.h"

ConcreteHandler2::ConcreteHandler2()
{

}

void ConcreteHandler2::handleRequest(const string &request)
{
    // 判断是否是自己处理的请求
    if (request.compare("two") == 0)
    {
        std::cout << "ConcreteHandler2 handle aging requests, key: " << request << std::endl;
        return;
    }

    // 将不是自己的请求通过父类方法传递给下一个
    IHandler::nextHandle(request);
}
