#ifndef CONCRETESUBJECT_H
#define CONCRETESUBJECT_H

#include <list>
using namespace std;
#include "isubject.h"

// 具体主题（Concrete Subject）角色
// 也叫具体目标类，它实现抽象目标中的通知方法，
// 当具体主题的内部状态发生改变时，通知所有注册过的观察者对象。
class ConcreteSubject : public ISubject
{
public:
    ConcreteSubject();

    //增加观察者方法
    void add(IObserver *observer) override;

    //删除观察者方法
    void remove(IObserver *observer) override;

    //通知观察者方法
    void notifyObserver() override;

private:
    list<IObserver*> mPObservers;
};

#endif // CONCRETESUBJECT_H
