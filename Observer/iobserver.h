#ifndef IOBSERVER_H
#define IOBSERVER_H

#include <string>
using namespace std;

// 抽象观察者（Observer）角色
// 它是一个抽象类或接口，它包含了一个更新自己的抽象方法，
// 当接到具体主题的更改通知时被调用。
class IObserver
{
public:
    virtual ~IObserver() {}

    //反应
    virtual void response() = 0;

    //设置唯一属性key
    virtual void setKey(const string &key) = 0;

    // 获取唯一属性key
    virtual const string getKey() = 0;
};

#endif // IOBSERVER_H
