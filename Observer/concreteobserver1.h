#ifndef CONCRETEOBSERVER1_H
#define CONCRETEOBSERVER1_H

#include "iobserver.h"

// 具体观察者（Concrete Observer）角色
// 实现抽象观察者中定义的抽象方法，以便在得到目标的更改通知时更新自身的状态。
class ConcreteObserver1 : public IObserver
{
public:
    ConcreteObserver1();

    void response() override;

    void setKey(const string &key) override;

    const string getKey() override;

private:
    string mKey;
};

#endif // CONCRETEOBSERVER1_H
