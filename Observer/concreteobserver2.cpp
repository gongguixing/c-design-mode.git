#include "concreteobserver2.h"
#include <iostream>

ConcreteObserver2::ConcreteObserver2()
{
    mKey = "";
}

void ConcreteObserver2::response()
{
    // 具体观察者2作出反应！
    std::cout << "I am the ConcreteObserver2." << std::endl;
}

void ConcreteObserver2::setKey(const string &key)
{
    mKey = key;
}

const string ConcreteObserver2::getKey()
{
    return mKey;
}
