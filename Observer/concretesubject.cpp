#include "concretesubject.h"
#include "iobserver.h"
#include <iostream>

ConcreteSubject::ConcreteSubject()
{
    mPObservers.clear();
}

void ConcreteSubject::add(IObserver *observer)
{
    if (!observer)
    {
        return ;
    }

    mPObservers.push_back(observer);
    std::cout << "ConcreteSubject add IObserver, key: ";
    std::cout << observer->getKey() << std::endl;
}

void ConcreteSubject::remove(IObserver *observer)
{
    if (!observer)
    {
        return ;
    }

    std::cout << "ConcreteSubject remove IObserver, key: ";
    std::cout << observer->getKey() << std::endl;

    for (auto it = mPObservers.begin(); it != mPObservers.end(); it++)
    {
        if (*it == observer)
        {
            mPObservers.erase(it);
            return;
        }
    }
}

void ConcreteSubject::notifyObserver()
{
    for (IObserver *observer : mPObservers)
    {
        observer->response();
    }
}
