#ifndef CONCRETEOBSERVER2_H
#define CONCRETEOBSERVER2_H

#include "iobserver.h"

// 具体观察者（Concrete Observer）角色
// 实现抽象观察者中定义的抽象方法，以便在得到目标的更改通知时更新自身的状态。
class ConcreteObserver2 : public IObserver
{
public:
    ConcreteObserver2();

    void response() override;

    void setKey(const string &key) override;

    const string getKey() override;

private:
    string mKey;
};

#endif // CONCRETEOBSERVER2_H
