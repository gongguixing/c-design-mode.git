#include "concreteobserver1.h"
#include <iostream>

ConcreteObserver1::ConcreteObserver1()
{
    mKey = "";
}

void ConcreteObserver1::response()
{
    // 具体观察者1作出反应！
    std::cout << "I am the ConcreteObserver1." << std::endl;
}

void ConcreteObserver1::setKey(const string &key)
{
    mKey = key;
}

const string ConcreteObserver1::getKey()
{
    return mKey;
}
