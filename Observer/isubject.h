#ifndef ISUBJECT_H
#define ISUBJECT_H

class IObserver;

// 抽象主题（Subject）角色
// 也叫抽象目标类，它提供了一个用于保存观察者对象的聚集类和增加、
// 删除观察者对象的方法，以及通知所有观察者的抽象方法。
class ISubject
{
public:
    virtual ~ISubject() {}

    //增加观察者方法
    virtual void add(IObserver *observer) = 0;

    //删除观察者方法
    virtual void remove(IObserver *observer) = 0;

    //通知观察者方法
    virtual void notifyObserver()   = 0;
};

#endif // ISUBJECT_H
