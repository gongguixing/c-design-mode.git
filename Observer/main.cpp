#include <iostream>
#include "concreteobserver1.h"
#include "concreteobserver2.h"
#include "concretesubject.h"

int main()
{
    // 观察者（Observer）模式的定义：指多个对象间存在一对多的依赖关系，
    // 当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新。
    // 这种模式有时又称作发布-订阅模式、模型-视图模式，它是对象行为型模式。

    // 实例化观察者
    IObserver * pO1 = new ConcreteObserver1();
    pO1->setKey("Observer1");
    IObserver * pO2 = new ConcreteObserver2();
    pO2->setKey("Observer2");
    // 实例化主题
    ISubject * pSub = new ConcreteSubject();
    // 订阅主题
    pSub->add(pO1);
    pSub->add(pO2);
    // 通知观察者
    pSub->notifyObserver();

    // 释放对象
    delete pSub;
    delete pO1;
    delete pO2;

    return 0;
}
