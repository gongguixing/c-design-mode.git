#include <iostream>
#include "flyweightfactory.h"

int main()
{
    // 实例化享元工厂（FlyweightFactory）角色
    FlyweightFactory * pFF = new FlyweightFactory();
    // 获取属性为a的享元对象
    IFlyweight *f01 = pFF->getFlyweight("a");
    // 获取属性为a的享元对象
    IFlyweight *f02 = pFF->getFlyweight("a");
    // 获取属性为a的享元对象
    IFlyweight *f03 = pFF->getFlyweight("a");
    // 获取属性为b的享元对象
    IFlyweight *f11 = pFF->getFlyweight("b");
    // 获取属性为b的享元对象
    IFlyweight *f12 = pFF->getFlyweight("b");

    // 调用享元对象a，传入参数为非享元对象（属性：The first call a.）
    f01->operation(UnsharedConcreteFlyweight("The first call a."));
    // 调用享元对象a，传入参数为非享元对象（属性：The second call a.）
    f02->operation(UnsharedConcreteFlyweight("The second call a."));
    // 调用享元对象a，传入参数为非享元对象（属性：The third call a.）
    f03->operation(UnsharedConcreteFlyweight("The third call a."));
    // 调用享元对象b，传入参数为非享元对象（属性：The first call b.）
    f11->operation(UnsharedConcreteFlyweight("The first call b"));
    // 调用享元对象b，传入参数为非享元对象（属性：The second call b.）
    f12->operation(UnsharedConcreteFlyweight("The second call b"));

    // 释放享元工厂
    delete pFF;
    return 0;
}
