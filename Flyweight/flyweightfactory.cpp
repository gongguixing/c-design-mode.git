#include "flyweightfactory.h"
#include "concreteflyweight.h"
#include<iostream>

FlyweightFactory::FlyweightFactory()
{
    mFlyweights = map<string, IFlyweight*>();
}

FlyweightFactory::~FlyweightFactory()
{
    // 释放指针对象
    for (pair<const string, IFlyweight*> it : mFlyweights)
    {
        delete it.second;
        it.second = NULL;
    }
}

IFlyweight *FlyweightFactory::getFlyweight(const string &key)
{
    //在容器中查找享元对象是否存在
    map<string, IFlyweight*>::iterator it = mFlyweights.find(key);
    IFlyweight *p = NULL;
    // 未找到享元对象，则创建；并存入容器
    if (it == mFlyweights.end())
    {
        p = new ConcreteFlyweight(key);
        mFlyweights.insert(pair<string, IFlyweight*>(key, p));
    }
    else // 已找到享元对象，则直接使用
    {
        p = it->second;
        std::cout << "ConcreteFlyweight : " << key << " is exist." << std::endl;
    }

    return p;
}
