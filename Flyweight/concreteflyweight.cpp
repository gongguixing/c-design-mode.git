#include "concreteflyweight.h"
#include <iostream>

ConcreteFlyweight::ConcreteFlyweight(const string &key)
{
    mKey = key;
    std::cout << "ConcreteFlyweight " << mKey << " build!" << std::endl;
}

void ConcreteFlyweight::operation(const UnsharedConcreteFlyweight &state)
{
    std::cout << "ConcreteFlyweight " << mKey << " used!" << std::endl;
    std::cout << "UnsharedConcreteFlyweight : " << state.getInfo() << std::endl;
}
