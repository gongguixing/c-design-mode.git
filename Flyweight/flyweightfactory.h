#ifndef FLYWEIGHTFACTORY_H
#define FLYWEIGHTFACTORY_H

#include "iflyweight.h"
#include <map>

//享元工厂角色
//享元工厂（Flyweight Factory）角色：负责创建和管理享元角色。
//当客户对象请求一个享元对象时，享元工厂检査系统中是否存在符合要求的享元对象，
//如果存在则提供给客户；如果不存在的话，则创建一个新的享元对象。
class FlyweightFactory
{
public:
    FlyweightFactory();
    ~FlyweightFactory();
    // 获取享元对象，key享元属性
    IFlyweight * getFlyweight(const string &key);

private:
    //存储享元对象容器
    map<string, IFlyweight*> mFlyweights;
};

#endif // FLYWEIGHTFACTORY_H
