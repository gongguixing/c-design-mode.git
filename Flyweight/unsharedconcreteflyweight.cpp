#include "unsharedconcreteflyweight.h"

UnsharedConcreteFlyweight::UnsharedConcreteFlyweight(const string &info)
    : mInfo(info)
{

}

void UnsharedConcreteFlyweight::setInfo(const string &info)
{
    mInfo = info;
}

string UnsharedConcreteFlyweight::getInfo() const
{
    return mInfo;
}
