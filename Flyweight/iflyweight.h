#ifndef IFLYWEIGHT_H
#define IFLYWEIGHT_H

#include "unsharedconcreteflyweight.h"

//抽象享元角色
//抽象享元角色（Flyweight）：是所有的具体享元类的基类，
//为具体享元规范需要实现的公共接口，非享元的外部状态以参数的形式通过方法传入
class IFlyweight
{
public:
    virtual ~IFlyweight() {}
    // 调用享元对象，传入参数为非享元对象
    virtual void operation(const UnsharedConcreteFlyweight &state) = 0;
};

#endif // IFLYWEIGHT_H
