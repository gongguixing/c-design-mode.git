#ifndef CONCRETEFLYWEIGHT_H
#define CONCRETEFLYWEIGHT_H

#include "iflyweight.h"

//具体享元角色
//具体享元（Concrete Flyweight）角色：实现抽象享元角色中所规定的接口。
class ConcreteFlyweight : public IFlyweight
{
public:
    //创建享元对象，属性为key
    ConcreteFlyweight(const string &key);
    // 调用享元对象，传入参数为非享元对象
    void operation(const UnsharedConcreteFlyweight &state) override;

private:
    string mKey;
};

#endif // CONCRETEFLYWEIGHT_H
