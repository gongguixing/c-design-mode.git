#ifndef UNSHAREDCONCRETEFLYWEIGHT_H
#define UNSHAREDCONCRETEFLYWEIGHT_H

#include <string>
using namespace std;

//非享元角色
//非享元（Unsharable Flyweight)角色：
//是不可以共享的外部状态，它以参数的形式注入具体享元的相关方法中。
class UnsharedConcreteFlyweight
{
public:
    // 创建非享元对象，属性为info
    UnsharedConcreteFlyweight(const string &info);

    // 设置非享元属性
    void setInfo(const string &info);
    // 获取非享元属性
    string getInfo() const;

private:
    string mInfo;
};

#endif // UNSHAREDCONCRETEFLYWEIGHT_H
