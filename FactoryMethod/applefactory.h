#ifndef APPLEFACTORY_H
#define APPLEFACTORY_H

#include "ifactory.h"

class AppleFactory : public IFactory
{
public:
    AppleFactory();
    IFruit * makeFruit() override;
};

#endif // APPLEFACTORY_H
