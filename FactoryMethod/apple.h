#ifndef APPLE_H
#define APPLE_H

#include "ifruit.h"

// 继承于水果基类
class Apple : public IFruit
{
public:
    Apple();

    // 重写show方法
    void show() override;
};

#endif // APPLE_H
