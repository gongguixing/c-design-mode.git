#include "bananafactory.h"
#include "banana.h"

BananaFactory::BananaFactory()
{

}

IFruit *BananaFactory::makeFruit()
{
    return new Banana();
}
