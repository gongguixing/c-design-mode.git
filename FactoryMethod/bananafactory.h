#ifndef BANANAFACTORY_H
#define BANANAFACTORY_H

#include "ifactory.h"

class BananaFactory : public IFactory
{
public:
    BananaFactory();
    IFruit * makeFruit() override;
};

#endif // BANANAFACTORY_H
