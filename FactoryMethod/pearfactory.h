#ifndef PEARFACTORY_H
#define PEARFACTORY_H

#include "ifactory.h"

class PearFactory : public IFactory
{
public:
    PearFactory();
    IFruit * makeFruit() override;
};

#endif // PEARFACTORY_H
