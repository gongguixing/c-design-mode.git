#ifndef IFRUIT_H
#define IFRUIT_H

// 水果基类
class IFruit
{
public:
    // 虚析构函数，动态绑定，方便在外部释放对象
    virtual ~IFruit() {}
    // 显示名称
    virtual void show() = 0;
};

#endif // IFRUIT_H
