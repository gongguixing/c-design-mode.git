#ifndef IFACTORY_H
#define IFACTORY_H

#include "ifruit.h"

class IFactory
{
public:
    virtual ~IFactory() {}

    virtual IFruit * makeFruit() = 0;
};

#endif // IFACTORY_H
