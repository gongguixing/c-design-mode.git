#include <iostream>
#include "applefactory.h"
#include "bananafactory.h"
#include "pearfactory.h"

int main()
{
    // 苹果工厂生产苹果
    IFactory *pAF = new AppleFactory();
    IFruit *pA = pAF->makeFruit();
    pA->show();

    // 香蕉工厂生产香蕉
    IFactory *pBF = new BananaFactory();
    IFruit *pB = pBF->makeFruit();
    pB->show();

    // 梨子工厂生产梨子
    IFactory *pCF = new PearFactory();
    IFruit *pC = pCF->makeFruit();
    pC->show();

    return 0;
}
