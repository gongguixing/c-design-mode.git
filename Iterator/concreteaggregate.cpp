#include "concreteaggregate.h"
#include <algorithm>
#include <iostream>
#include "concreteiterator.h"

ConcreteAggregate::ConcreteAggregate()
{
    mObjects.clear();
    mpIt = new ConcreteIterator(&mObjects);
}

ConcreteAggregate::~ConcreteAggregate()
{
    delete mpIt;
}

void ConcreteAggregate::add(const Object &obj)
{
    mObjects.push_back(obj);
}

void ConcreteAggregate::remove(const Object &obj)
{
    size_t size = mObjects.size();
    // 匿名函数，判断属性name是否已经存在
    auto fun = [obj](Object o)->bool{return o.name == obj.name;};
    // 移动赋值
    auto it = std::remove_if(mObjects.begin(), mObjects.end(), fun);
    // 删除
    mObjects.erase(it, mObjects.end());

    std::cout << "remove Object: " << obj.name << "  ";
    std::cout << (size == mObjects.size() ? "failure" : "success ") << std::endl;
}

Iterator *ConcreteAggregate::getIterator()
{
    return mpIt;
}
