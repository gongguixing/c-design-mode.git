#include <iostream>
#include "concreteaggregate.h"
#include "iterator.h"

int main()
{
    // 迭代器（Iterator）模式的定义：
    // 提供一个对象来顺序访问聚合对象中的一系列数据，
    // 而不暴露聚合对象的内部表示。

    // 实例化聚合角色
    IAggregate * pAg = new ConcreteAggregate();
    // 添加角色
    pAg->add(Object("Sun Yat-sen University"));
    pAg->add(Object("South China University of Technology"));
    pAg->add(Object("Nanjing University"));

    std::cout << "The contents of the aggregation are:" << std::endl;

    // 获取迭代器
    Iterator *it = pAg->getIterator();

    // 遍历迭代器
    while (it->hasNext())
    {
        Object * obj = it->next();
        std::cout << obj->name << std::endl;
    }

    std::cout << "--------------------------------------" << std::endl;

    // 访问迭代器首
    Object *fObj = it->first();
    std::cout << "first Object: " << fObj->name << std::endl;

    // 访问迭代器尾
    Object *lObj = it->last();
    std::cout << "last Object: " << lObj->name << std::endl;

    std::cout << "--------------------------------------" << std::endl;

    // 删除对象
    pAg->remove(Object("Sun Yat-sen University"));
    pAg->remove(Object("Sun Yat-sen University"));

    delete pAg;

    return 0;
}
