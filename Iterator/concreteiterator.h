#ifndef CONCRETEITERATOR_H
#define CONCRETEITERATOR_H

#include "iterator.h"
#include <vector>

// 具体迭代器（Concretelterator）角色
// 实现抽象迭代器接口中所定义的方法，
// 完成对聚合对象的遍历，记录遍历的当前位置。

class ConcreteIterator : public Iterator
{
public:
    ConcreteIterator(vector<Object> *pObjects);

    // 获取第一个对象
    Object * first() override;

    // 获取最后一个对象
    Object * last() override;

    // 获取下一个对象
    Object * next() override;

    // 是否存在下一个对象
    bool hasNext() override;

private:
    vector<Object> *mpObjects;
    int index = 0;
};

#endif // CONCRETEITERATOR_H
