#include "concreteiterator.h"

ConcreteIterator::ConcreteIterator(vector<Object> *pObjects)
{
    mpObjects = pObjects;
    index = -1;
}

Object *ConcreteIterator::first()
{
    if (!mpObjects)
    {
        return nullptr;
    }

    if (mpObjects->empty())
    {
        return nullptr;
    }

    index = 0;
    return &(mpObjects->at(index));
}

Object *ConcreteIterator::last()
{
    if (!mpObjects)
    {
        return nullptr;
    }

    if (mpObjects->empty())
    {
        return nullptr;
    }

    index = mpObjects->size() - 1;
    return &(mpObjects->at(index));
}

Object *ConcreteIterator::next()
{
    if (!mpObjects)
    {
        return nullptr;
    }

    Object * p = nullptr;

    // 如果存在下一个对象，则取出来
    if (hasNext())
    {
        p = &(mpObjects->at(++index));
    }

    return p;
}

bool ConcreteIterator::hasNext()
{
    return index < (int)(mpObjects->size() - 1) ? true : false;
}
