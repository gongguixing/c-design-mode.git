#ifndef ITERATOR_H
#define ITERATOR_H

#include "define.h"

// 抽象迭代器（Iterator）角色
// 定义访问和遍历聚合元素的接口，
// 通常包含 hasNext()、first()、next() 等方法。

class Iterator
{
public:
    virtual ~Iterator() {}

    // 获取第一个对象
    virtual Object * first() = 0;

    // 获取最后一个对象
    virtual Object * last() = 0;

    // 获取下一个对象
    virtual Object * next() = 0;

    // 是否存在下一个对象
    virtual bool hasNext() = 0;
};

#endif // ITERATOR_H
