#ifndef CONCRETEAGGREGATE_H
#define CONCRETEAGGREGATE_H

#include "iaggregate.h"
#include <vector>

// 具体聚合（ConcreteAggregate）角色
// 实现抽象聚合类，返回一个具体迭代器的实例。

class ConcreteAggregate : public IAggregate
{
public:
    ConcreteAggregate();
    ~ ConcreteAggregate();

    // 添加对象
    void add(const Object &obj) override;

    // 删除对象
    void remove(const Object &obj) override;

    // 获取迭代器
    Iterator * getIterator()  override;

private:
    vector<Object> mObjects;
    Iterator *mpIt;
};

#endif // CONCRETEAGGREGATE_H
