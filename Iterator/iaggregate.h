#ifndef IAGGREGATE_H
#define IAGGREGATE_H

#include "define.h"

class Iterator;

// 抽象聚合（Aggregate）角色
// 定义存储、添加、删除聚合对象以及创建迭代器对象的接口。

class IAggregate
{
public:
    virtual ~IAggregate() {}

    // 添加对象
    virtual void add(const Object &obj) = 0;

    // 删除对象
    virtual void remove(const Object &obj) = 0;

    // 迭获取代器
    virtual Iterator * getIterator()  = 0;
};

#endif // IAGGREGATE_H
