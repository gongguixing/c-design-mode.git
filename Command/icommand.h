#ifndef ICOMMAND_H
#define ICOMMAND_H

// 抽象命令类（Command）角色
// 声明执行命令的接口，拥有执行命令的抽象方法 execute()。
class ICommand
{
public:
    virtual ~ICommand() {}

    virtual void execute()  = 0;
};

#endif // ICOMMAND_H
