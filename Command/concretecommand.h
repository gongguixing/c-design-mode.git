#ifndef CONCRETECOMMAND_H
#define CONCRETECOMMAND_H

#include "icommand.h"

// 具体命令类（Concrete Command）角色
// 是抽象命令类的具体实现类，它拥有接收者对象，
// 并通过调用接收者的功能来完成命令要执行的操作。
class Receiver;

class ConcreteCommand : public ICommand
{
public:
    ConcreteCommand();

    void execute() override;

private:
    Receiver *mPReceiver;
};

#endif // CONCRETECOMMAND_H
