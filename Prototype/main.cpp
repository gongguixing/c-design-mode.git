#include "cprototype.h"
#include <iostream>

int main()
{
    // 调用默认构造函数，实例化对象
    CPrototype *p1 = new CPrototype();

    CPrototype *p2 = p1->Clone();

    // 拷贝构造函数会连属性一起拷贝
    std::cout << "p1 address: " << p1 << " count: " << p1->m_count << std::endl;
    std::cout << "p2 address: " << p2 << " count: " << p1->m_count << std::endl;

    delete p1;
    delete p2;

    return 0;
}
