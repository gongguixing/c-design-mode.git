#include "cprototype.h"
#include "iostream"

CPrototype::CPrototype(): m_count(1024)
{
    std::cout << "Default constructor" << std::endl;
}

CPrototype::~CPrototype()
{

}

CPrototype::CPrototype(const CPrototype &other)
{
    m_count = other.m_count;
    std::cout << "Explicit copy constructor" << std::endl;
}

CPrototype *CPrototype::Clone()
{
    return new CPrototype(*this);
}
