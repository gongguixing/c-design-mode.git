#ifndef CPROTOTYPE_H
#define CPROTOTYPE_H

class CPrototype
{
public:
    // 默认构造函数
    CPrototype();
    ~CPrototype();

    // 显式拷贝构造函数
    CPrototype(const CPrototype &other);

    CPrototype * Clone();

public:
    int m_count;
};

#endif // CPROTOTYPE_H
