#include <iostream>
#include "knife.h"
#include "ak47.h"
#include "character.h"

int main()
{
    // 策略（Strategy）模式的定义：
    // 该模式定义了一系列算法，并将每个算法封装起来，使它们可以相互替换，
    // 且算法的变化不会影响使用算法的客户。策略模式属于对象行为模式，
    // 它通过对算法进行封装，把使用算法的责任和算法的实现分割开来，
    // 并委派给不同的对象对这些算法进行管理。

    // 环境（Context）类
    Character * pTer = new Character();
    // 具体策略类：匕首
    IWeaponStrategy * pKnife = new Knife();
    // 环境类设置策略
    pTer->setWeapon(pKnife);
    // 环境类调用策略方法
    pTer->throwWeapon();

    std::cout << std::endl;

    // 具体策略类：AK47
    IWeaponStrategy * PAK47 = new AK47();
    // 环境类设置策略
    pTer->setWeapon(PAK47);
    // 环境类调用策略方法
    pTer->throwWeapon();

    delete pKnife;
    delete PAK47;
    delete pTer;

    return 0;
}
