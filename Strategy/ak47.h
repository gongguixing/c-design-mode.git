#ifndef AK47_H
#define AK47_H

#include "iweaponstrategy.h"

// 具体策略（Concrete Strategy）类
// 实现了抽象策略定义的接口，提供具体的算法实现。
class AK47 : public IWeaponStrategy
{
public:
    AK47();

    // 以自己的方式实现父类抽象接口
    void useWeapon() override;
};

#endif // AK47_H
