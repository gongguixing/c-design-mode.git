#ifndef CHARACTER_H
#define CHARACTER_H

#include "iweaponstrategy.h"

// 环境（Context）类
// 持有一个策略类的引用，最终给客户端调用。
class Character
{
public:
    Character();

    // 设置策略类
    void setWeapon(IWeaponStrategy * p);
    // 内部实现策略类的调用
    void throwWeapon();
private:
    IWeaponStrategy *mPWeapon;
};

#endif // CHARACTER_H
