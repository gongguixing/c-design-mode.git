#include "character.h"

Character::Character()
{
    mPWeapon = nullptr;
}

void Character::setWeapon(IWeaponStrategy *p)
{
    mPWeapon = p;
}

void Character::throwWeapon()
{
    // 策略类调用
    if (mPWeapon)
    {
        mPWeapon->useWeapon();
    }
}
