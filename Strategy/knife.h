#ifndef KNIFE_H
#define KNIFE_H

#include "iweaponstrategy.h"

// 具体策略（Concrete Strategy）类
// 实现了抽象策略定义的接口，提供具体的算法实现。
class Knife : public IWeaponStrategy
{
public:
    Knife();

    // 以自己的方式实现父类抽象接口
    void useWeapon() override;
};

#endif // KNIFE_H
