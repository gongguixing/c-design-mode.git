#include <iostream>
#include "context.h"

int main()
{
    //设置字符集，终端正常显示中文
    system("chcp 65001");

    Context * pBus = new Context();
    pBus->freeRide("珠海的老人");
    pBus->freeRide("珠海的年轻人");
    pBus->freeRide("广州的妇女");
    pBus->freeRide("广州的儿童");
    pBus->freeRide("山东的儿童");

    delete pBus;

    return 0;
}
