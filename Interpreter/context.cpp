#include "context.h"
#include <iostream>

Context::Context()
{
    mCitys = {"广州", "珠海"};
    mPersons = {"老人", "妇女", "儿童"};

    pCity       = new TerminalExpression(mCitys);
    pPerson     = new TerminalExpression(mPersons);
    pCityPerson = new AndExpression(pCity, pCityPerson);
}

Context::~Context()
{
    delete pCityPerson;
    delete pCity;
    delete pPerson;
}

void Context::freeRide(const string &info)
{
    if (pCityPerson->interpret(info))
    {
        std::cout << "您是" << info << "，您本次乘车免费！" << std::endl;
    }
    else
    {
        std::cout << info << "，您不是免费人员，本次乘车扣费2元！" << std::endl;
    }
}
