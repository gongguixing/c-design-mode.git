#ifndef IEXPRESSION_H
#define IEXPRESSION_H

#include <string>
#include <vector>
using namespace std;

// 抽象表达式（Abstract Expression）角色
// 定义解释器的接口，约定解释器的解释操作，主要包含解释方法 interpret()。

class IExpression
{
public:
    virtual ~IExpression() {}

    virtual bool interpret(const string &info)  = 0;
};

#endif // IEXPRESSION_H
