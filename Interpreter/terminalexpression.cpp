#include "terminalexpression.h"
#include <algorithm>

TerminalExpression::TerminalExpression(const vector<string> &infos)
    : mInfos(infos)
{

}

bool TerminalExpression::interpret(const string &info)
{
    auto it = std::find(mInfos.begin(), mInfos.end(), info);
    return it == mInfos.end() ? false : true;
}
