#ifndef CONTEXT_H
#define CONTEXT_H

#include "andexpression.h"
#include "terminalexpression.h"

class Context
{
public:
    Context();
    ~Context();

    void freeRide(const string &info);

private:
    vector<string> mCitys;
    vector<string> mPersons;

    IExpression * pCity;
    IExpression * pPerson;

    IExpression * pCityPerson;
};

#endif // CONTEXT_H
