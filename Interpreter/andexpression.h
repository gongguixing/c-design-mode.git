#ifndef ANDEXPRESSION_H
#define ANDEXPRESSION_H

#include "iexpression.h"

// 非终结符表达式（Nonterminal Expression）角色
// 也是抽象表达式的子类，用来实现文法中与非终结符相关的操作，
// 文法中的每条规则都对应于一个非终结符表达式。

class AndExpression : public IExpression
{
public:
    AndExpression(IExpression * pCity, IExpression * pPerson);

    bool interpret(const string &info) override;

    // 字符串分割
    size_t split(const string &info, vector<string> &infos);

private:
    IExpression * pCity;
    IExpression * pPerson;
};

#endif // ANDEXPRESSION_H
