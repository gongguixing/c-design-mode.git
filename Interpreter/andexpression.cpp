#include "andexpression.h"

AndExpression::AndExpression(IExpression *pCity, IExpression *pPerson)
{
    this->pCity = pCity;
    this->pPerson = pPerson;
}

bool AndExpression::interpret(const string &info)
{
    vector<string> infos;
    if (split(info, infos) != 2)
    {
        return false;
    }

    return pCity->interpret(infos.at(0)) && pPerson->interpret(infos.at(1));
}

size_t AndExpression::split(const string &info, vector<string> &infos)
{
    // 分割字符
    string spStr = "的";
    // 起始位置
    size_t pos = 0;

    while (true)
    {
        size_t npos = info.find(spStr, pos);
        // 未找到分割字符
        if (npos == string::npos)
        {
            infos.push_back(info.substr(pos, info.size() - 1));
            break;
        }
        // 截取字符串
        string str = info.substr(pos, npos);
        // 定义下个位置
        pos = npos + spStr.size();
        infos.push_back(str);
    }

    return info.size();
}
