#ifndef TERMINALEXPRESSION_H
#define TERMINALEXPRESSION_H

#include "iexpression.h"

// 终结符表达式（Terminal Expression）角色
// 是抽象表达式的子类，用来实现文法中与终结符相关的操作，
// 文法中的每一个终结符都有一个具体终结表达式与之相对应。

class TerminalExpression : public IExpression
{
public:
    TerminalExpression(const vector<string> &infos);

    bool interpret(const string &info) override;

private:
    vector<string> mInfos;
};

#endif // TERMINALEXPRESSION_H
