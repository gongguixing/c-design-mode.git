#ifndef IFACTORY_H
#define IFACTORY_H

#include "iapple.h"
#include "ibanana.h"

// 抽象工厂，可以生产不同等级的产品
class IFactory
{
public:
    virtual ~IFactory() {}
    virtual IApple * makeApple() = 0;
    virtual IBanana * makeBanana() = 0;
};

#endif // IFACTORY_H
