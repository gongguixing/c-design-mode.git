#ifndef GXBANANA_H
#define GXBANANA_H

#include "ibanana.h"

// 某个产品的实现
class GXBanana : public IBanana
{
public:
    GXBanana();

    void bShow() override;
};

#endif // GXBANANA_H
