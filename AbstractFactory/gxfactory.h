#ifndef GXFACTORY_H
#define GXFACTORY_H

#include "ifactory.h"

class GXFactory : public IFactory
{
public:
    GXFactory();
    IApple * makeApple();
    IBanana * makeBanana();
};

#endif // GXFACTORY_H
