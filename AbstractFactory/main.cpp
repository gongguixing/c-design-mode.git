#include <iostream>
#include "gdfactory.h"
#include "gxfactory.h"

int main()
{
    IFactory *pGDF = new GDFactory();
    IApple *pGDA = pGDF->makeApple();
    pGDA->aShow();
    IBanana *pGDB = pGDF->makeBanana();
    pGDB->bShow();

    IFactory *pGXF = new GXFactory();
    IApple *pGXA = pGXF->makeApple();
    pGXA->aShow();
    IBanana *pGXB = pGXF->makeBanana();
    pGXB->bShow();

    return 0;
}
