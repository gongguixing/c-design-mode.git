#ifndef GDFACTORY_H
#define GDFACTORY_H

#include "ifactory.h"

class GDFactory : public IFactory
{
public:
    GDFactory();

    IApple * makeApple();
    IBanana * makeBanana();
};

#endif // GDFACTORY_H
