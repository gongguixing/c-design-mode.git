#include "gdfactory.h"
#include "gdapple.h"
#include "gdbanana.h"

GDFactory::GDFactory()
{

}

IApple *GDFactory::makeApple()
{
    return new GDApple();
}

IBanana *GDFactory::makeBanana()
{
    return new GDBanana();
}
