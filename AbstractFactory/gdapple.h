#ifndef GDAPPLE_H
#define GDAPPLE_H

#include "iapple.h"

class GDApple : public IApple
{
public:
    GDApple();
    void aShow() override;
};

#endif // GDAPPLE_H
