#ifndef IBANANA_H
#define IBANANA_H

// 某个等级的抽象产品

class IBanana
{
public:
    virtual ~IBanana() {}

    virtual void bShow() = 0;
};

#endif // IBANANA_H
