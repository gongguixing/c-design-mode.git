#include "gxfactory.h"
#include "gxapple.h"
#include "gxbanana.h"

GXFactory::GXFactory()
{

}

IApple *GXFactory::makeApple()
{
    return new GXApple();
}

IBanana *GXFactory::makeBanana()
{
    return new GXBanana();
}
