#ifndef GXAPPLE_H
#define GXAPPLE_H

#include "iapple.h"

class GXApple : public IApple
{
public:
    GXApple();

    void aShow() override;
};

#endif // GXAPPLE_H
