#ifndef GDBANANA_H
#define GDBANANA_H

#include "ibanana.h"

// 某个产品的实现
class GDBanana : public IBanana
{
public:
    GDBanana();

    void bShow() override;
};

#endif // GDBANANA_H
