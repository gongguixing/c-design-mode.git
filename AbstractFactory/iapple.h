#ifndef IAPPLE_H
#define IAPPLE_H

// 某个等级的抽象产品
class IApple
{
public:
    virtual ~ IApple() {}
    virtual void aShow() = 0;
};

#endif // IAPPLE_H
