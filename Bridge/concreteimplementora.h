#ifndef CONCRETEIMPLEMENTORA_H
#define CONCRETEIMPLEMENTORA_H

#include "implementor.h"

class ConcreteImplementorA : public Implementor
{
public:
    ConcreteImplementorA();

    void OperationImpl() override;
};

#endif // CONCRETEIMPLEMENTORA_H
