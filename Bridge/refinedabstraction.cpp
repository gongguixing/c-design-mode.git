#include "refinedabstraction.h"
#include <iostream>

RefinedAbstraction::RefinedAbstraction()
{
    m_Imple = NULL;
}

void RefinedAbstraction::setImple(Implementor *p)
{
    m_Imple = p;
}

void RefinedAbstraction::Operation()
{
    std::cout << "I am the RefinedAbstraction." << std::endl;
    m_Imple->OperationImpl();
}
