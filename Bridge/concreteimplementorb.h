#ifndef CONCRETEIMPLEMENTORB_H
#define CONCRETEIMPLEMENTORB_H

#include "implementor.h"

class ConcreteImplementorB : public Implementor
{
public:
    ConcreteImplementorB();

    void OperationImpl() override;
};

#endif // CONCRETEIMPLEMENTORB_H
