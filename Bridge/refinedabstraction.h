#ifndef REFINEDABSTRACTION_H
#define REFINEDABSTRACTION_H

#include "abstraction.h"

class RefinedAbstraction : public Abstraction
{
public:
    RefinedAbstraction();

    void setImple(Implementor *p) override;
    void Operation() override;
private:
    Implementor *m_Imple;
};

#endif // REFINEDABSTRACTION_H
