#include <iostream>
#include "concreteimplementora.h"
#include "concreteimplementorb.h"
#include "refinedabstraction.h"

int main()
{
    Implementor * pImple = new ConcreteImplementorA();
    Abstraction * pAbs = new RefinedAbstraction();
    pAbs->setImple(pImple);
    pAbs->Operation();
    delete pImple;
    pImple = NULL;
    std::cout << std::endl;

    pImple = new ConcreteImplementorB();
    pAbs->setImple(pImple);
    pAbs->Operation();
    delete pImple;
    delete pAbs;

    return 0;
}
