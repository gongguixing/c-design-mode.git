#ifndef ABSTRACTION_H
#define ABSTRACTION_H

#include "implementor.h"

class Abstraction
{
public:
    virtual ~ Abstraction() {}

    virtual void setImple(Implementor *p) = 0;
    virtual void Operation() = 0;
};

#endif // ABSTRACTION_H
