#ifndef IMPLEMENTOR_H
#define IMPLEMENTOR_H


class Implementor
{
public:
    virtual ~Implementor() {}
    virtual void OperationImpl() = 0;
};

#endif // IMPLEMENTOR_H
