#include "clazysingleton.h"
#include "iostream"

std::mutex CLazySingleton::g_mutex;

CLazySingleton::CLazySingleton()
{
    // 打印字符，观察对象合时被实例化，被实例化多少次？
    std::cout << "constructor" << std::endl;
}

CLazySingleton::~CLazySingleton()
{

}

//CLazySingleton *CLazySingleton::instance()
//{
//    static CLazySingleton singleton;
//    return &singleton;
//}

//CLazySingleton *CLazySingleton::instance()
//{
//    static CLazySingleton * p = NULL;
//    if (NULL == p)
//    {
//        g_mutex.lock();
//        if (NULL == p)
//        {
//            p = new CLazySingleton();
//        }
//        g_mutex.unlock();
//    }
//    return p;
//}

//CLazySingleton *CLazySingleton::instance()
//{
//    static CLazySingleton * p = NULL;
//    g_mutex.lock();
//    if (NULL == p)
//    {
//        p = new CLazySingleton();
//    }
//    g_mutex.unlock();
//    return p;
//}

//CLazySingleton *CLazySingleton::instance()
//{
//    static CLazySingleton * p = NULL;
//    if (NULL == p)
//    {
//        p = new CLazySingleton();
//    }

//    return p;
//}

CLazySingleton *CLazySingleton::instance()
{
    // 类内创建对象
    // 使用静态对象，保证对象只会被创建一次
    // 此方法已经线程安全
    std::cout << "instance" << std::endl;
    static CLazySingleton * p = new CLazySingleton();
    return p;
}

void CLazySingleton::say()
{
    std::cout << "I am LazySingleton!" << std::endl;
}
