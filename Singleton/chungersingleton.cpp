#include "chungersingleton.h"
#include "iostream"

// 对象在cpp文件构造，禁止外部直接访问
//CHungerSingleton * CHungerSingleton::g_p = new CHungerSingleton();

CHungerSingleton::CHungerSingleton()
{
    // 打印字符，观察对象合时被实例化，被实例化多少次？
    std::cout << "constructor" << std::endl;
}

CHungerSingleton::~CHungerSingleton()
{

}

CHungerSingleton *CHungerSingleton::instance()
{
    // 对象已经创建好必然，线程安全
    std::cout << "instance" << std::endl;
    return NULL;
}

void CHungerSingleton::say()
{
    std::cout << "I am HungerSingleton!" << std::endl;
}
