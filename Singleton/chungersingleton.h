#ifndef CHUNGERSINGLETON_H
#define CHUNGERSINGLETON_H


class CHungerSingleton
{
private:
    // 构造函数私有，禁止外部创建，释放
    CHungerSingleton();
    ~CHungerSingleton();

private:
    // 静态成员变量指针私有
    static CHungerSingleton *g_p;

public:
    // 静态方法，全局访问
    static CHungerSingleton * instance();
    void say();
};

#endif // CHUNGERSINGLETON_H
