#ifndef CLAZYSINGLETON_H
#define CLAZYSINGLETON_H

#include "mutex"

class CLazySingleton
{

private:
    // 构造函数私有，禁止外部创建，释放
    CLazySingleton();
    ~CLazySingleton();
public:
    // 静态方法，全局访问
    static CLazySingleton * instance();
    void say();

private:
    static std::mutex g_mutex;
};

#endif // CLAZYSINGLETON_H
