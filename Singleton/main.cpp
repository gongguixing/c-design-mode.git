#include <iostream>
#include "clazysingleton.h"
#include "chungersingleton.h"

void lazySingleton()
{
    // 获取对象指针再执行say
    CLazySingleton::instance()->say();

    // 即使多次调用也不会创建多个对象
    std::cout << "Object address 1: " << CLazySingleton::instance() << std::endl;
    std::cout << "Object address 2: " << CLazySingleton::instance() << std::endl;
}


void hungerSingleton()
{
    // 获取对象指针再执行say
    CHungerSingleton::instance()->say();

    // 即使多次调用也不会创建多个对象
    std::cout << "Object address 1: " << CHungerSingleton::instance() << std::endl;
    std::cout << "Object address 2: " << CHungerSingleton::instance() << std::endl;
}


int main()
{
    std::cout << "main begin" << std::endl;
    lazySingleton();
//    hungerSingleton();
    return 0;
}
